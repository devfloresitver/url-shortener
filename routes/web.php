<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/','URLController@index');
Route::post('/generate','URLController@generate');
Route::get('/myLinks', 'URLController@myLinks')->middleware('auth')->name('myLinks');
Route::delete('/delete/{id}', 'URLController@deleteLink')->middleware('auth');


Route::get('go/{originalURL}', 'URLController@redirectToOriginalURL')->name('redirect');

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
