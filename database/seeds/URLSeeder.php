<?php

use Illuminate\Database\Seeder;
use App\Url;
use App\User;

class URLSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class,20)->create();
        factory(Url::class,20)->create();
       
    }
}
