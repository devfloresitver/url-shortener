<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Url;
use App\User;
use Faker\Generator as Faker;

$factory->define(Url::class, function (Faker $faker) {
	 $users = App\User::pluck('id')->toArray();
    return [
    	'original' => $faker->url,
    	'generated' => $faker->url,
    	'user_id' => $faker->randomElement($users)
    ];
});
