<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    protected $dates = ['fecha'];

    public function user(){//uno a muchos
    	return $this->belongsTo(User::class);
    }
}
