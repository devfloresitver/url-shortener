<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\URL_Imp;
use App\Url;
//use Request;
class URLController extends Controller
{
    public function index(){
    	return view('home');
    }

    public function generate(Request $request){

        //retrieve client data
        $plainURL = $request->url;

        //generate shortened links
        $url = new URL_Imp($plainURL);
        $shortenedURL = $url->shortenURL();

        //store a new record via eloquent
        $newURL = new Url();
        $newURL->original = $plainURL;
        $newURL->generated = $shortenedURL;

        if($plainURL!==''){

            $userId = auth()->id() != null ? auth()->id() : 0;
            if($userId > 1){

                $newURL->user_id = $userId;

            }
            else{
                $newURL->user_id = 1;
            }

        $newURL->save();

        //return the shortened link
    	return $newURL;

        }
    }

    public function myLinks(Request $request){
        if($request->ajax()){
            return Url::where('user_id', auth()->id())->get();
        }else{
            return view('links');
        }
    }

    public function deleteLink($id){
        $flaggedUrl = Url::find($id);
        $flaggedUrl->delete();
    }

    public function redirectToOriginalURL($generatedCode){
        //go/--
        if($generatedCode == null){
            abort(500, 'ERROR');
        }
        $originalUrl = Url::where('generated', "like", '%'.$generatedCode)
        ->get()[0]->original;
        return redirect()->away($originalUrl);

    }


}
